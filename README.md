### quickMusicPlayer() plugin

- **Description:** from an `<audio>` selector, generate a music player with the following:
  - play/pause buttons
  - current timestamp
  - song duration slider (clickable, draggable)
  - total duration
- **Requirements:** intermediate HTML/CSS knowledge (requires substantial amount of customization and editing; this plugin mostly grants functionality over style).
- **Editing & Customization:** you're welcome to `inspect element` as much as you want to get an overview of the markup, to see what the class names are if you want to edit them.
- **Author:** HT (@&#x200A;glenthemes)

#### Preview (unstyled):
![2 rows, each presenting 1 song. Each row, from left to right, contains a pause button that will turn to a play icon when clicked, a text display of the current timestamp, a slider representing the song length that is currently filled at the corresponding timestamp, and a text display of the total duration.](https://github.com/ht-devx/ht-devx.github.io/assets/162752471/5348b702-bf26-4fce-b18c-a07caf1f3d05)

#### Preview (stylized):
![2 rows, each presenting 1 song. Each row, from left to right, contains a black-and-white image of a disc, the song name and artist name, a pause button that will turn to a play icon when clicked, a text display of the current timestamp, a slider representing the song length that is currently filled at the corresponding timestamp, and a text display of the total duration.](https://github.com/ht-devx/ht-devx.github.io/assets/162752471/f1c0cea3-5cce-4137-beca-1725435c1280)

#### Demo:
:musical_score:&ensp;[jsfiddle.net/glenthemes/k9Ldwha1](https://jsfiddle.net/glenthemes/s237Lbu4)  
:point_up_tone2: <small>(stylized version)</small>

---

#### Step 1:
Paste the following in any of these locations:
- between `<head>` and `</head>`
- between `<body>` and `</body>`
```html
<!--✻✻✻✻✻✻  quick music player by @glenthemes  ✻✻✻✻✻✻-->
<script src="https://quick-music-player.gitlab.io/q/init.js"></script>
<link href="https://quick-music-player.gitlab.io/q/ly.css" rel="stylesheet">
<script>
quickMusicPlayer({
	container: ".container", // parent or ancestor of your <audio>
	audio: "audio", // selector of your <audio>
	playIcon: '<i class="mi-play"></i>', // play icon that'll be created for you
	pauseIcon: '<i class="ti ti-player-pause"></i>' // pause icon that'll be created for you
})
</script>

<!-- load the icons (play & pause) -->
<link href="https://unpkg.com/mono-icons@latest/iconfont/icons.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/tabler-icons.min.css" rel="stylesheet">
```

| Option Name | Explanation |
| ----------- | ----------- |
| `container` | An **existing** selector that contains your `<audio>` |
| `audio` | An **existing** selector that **is** your `<audio>` |
| `playIcon` and `pauseIcon` | The icon code that will be generated for you that serves as the player controls. Can be an icon font or a SVG. If you choose to use a library other than the one(s) provided here, make sure to include them in your project. |

#### Step 2:
Add this CSS (inside a `<style>`):
```css
:root {	
	--Quick-Music-Player-Buttons-Touch-Padding:5px;
	
	--Quick-Music-Player-Slider-Height:11px;
	--Quick-Music-Player-Slider-BG:#f2f2f2;
	--Quick-Music-Player-Slider-Corner-Roundness:5px;
	--Quick-Music-Player-Slider-Fill-Color:#222;
	--Quick-Music-Player-Slider-Gaps:10px;
	--Quick-Music-Player-Slider-Touch-Padding:5px;
	
	--Quick-Music-Player-Knob-Width:4px;
	--Quick-Music-Player-Knob-Height:18px;
	--Quick-Music-Player-Knob-Color:#222;
}
```
※&ensp;The `Touch-Padding` options refer to the bonus area you grant your users to be able to interact with something, so that it's easier for them to click or drag in case your settings are a bit small for them.

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this useful?
Consider leaving me a tip over at [ko-fi.com/glenthemes](https://ko-fi.com/glenthemes)!  
Thank you :sparkles:
