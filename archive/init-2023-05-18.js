/*----------------------------------------------
    
    quick music player w/ slider: <audio>
    - written by HT (@glenthemes)
    
----------------------------------------------*/

window.quickMusicPlayer = function(e_e){
	let ps = e_e.audio.replaceAll(", ",",");
	let pc = e_e.container;
	let playIcon = e_e.playIcon;
	let pauseIcon = e_e.pauseIcon;
	ps = ps.split(",");
	document.querySelector(ps) !== null && 
	document.querySelector(ps).matches("[src]") && 
	document.querySelector(ps).matches("audio") && 
	document.querySelector(ps).src.trim() !== "" &&
	document.querySelector(pc) !== null ?
	document.querySelectorAll(ps).forEach(pps => {
		let audioSrc = pps.src;
		let audioTag = pps;
		let audioVol;
        let audioDur;
		
		// wrap <audio> in a container
		let newAudCont = document.createElement("div");
		newAudCont.classList.add("q-music-player-wrap");
		audioTag.before(newAudCont);
		newAudCont.append(audioTag);
		
		/*----- BUTTONS -----*/
		
		// create music player buttons
		let addCtrls = document.createElement("div");
		addCtrls.classList.add("q-controls");
		newAudCont.prepend(addCtrls)
		
		// create play button
		let playBtn = document.createElement("div");
		playBtn.classList.add("q-play");
		playBtn.innerHTML = playIcon;
		addCtrls.append(playBtn);
		
		// create pause button
		let pauseBtn = document.createElement("div");
		pauseBtn.classList.add("q-pause");
		pauseBtn.style.visibility = "hidden";
		pauseBtn.innerHTML = pauseIcon;
		addCtrls.append(pauseBtn);
		
		setTimeout(() => {
			pauseBtn.style.visibility = "";
			pauseBtn.style.display = "none";
		},0)
		
		/*----- SLIDER -----*/
		
		// create a slider - container
		let sliderCont = document.createElement("div");
		sliderCont.classList.add("q-slider-wrap");
		newAudCont.append(sliderCont);
		
		// create a slider - empty bar
		let sliderMT = document.createElement("div");
		sliderMT.classList.add("q-slider-bar");
		sliderCont.append(sliderMT);
		
		// create a slider - filled part
		let sliderFill = document.createElement("div");
		sliderFill.classList.add("q-slider-fill");
		sliderCont.append(sliderFill)
		
		// create a slider - knob container
		let sliderKnobCont = document.createElement("div");
		sliderKnobCont.classList.add("q-slider-knob-wrap");
		newAudCont.append(sliderKnobCont)
		
		// create a slider - knob
		let sliderKnob = document.createElement("div");
		sliderKnob.classList.add("q-slider-knob");
		sliderKnobCont.append(sliderKnob)
		
		// group SLIDER and KNOB in one container
		let mcWrap = document.createElement("div");
		mcWrap.classList.add("q-mcwrap");
		newAudCont.append(mcWrap)
		
		mcWrap.append(sliderCont)
		mcWrap.append(sliderKnobCont)
		
		/*----- ONLOAD -----*/
		let widthToSongRatio;
		
		audioTag.addEventListener("loadedmetadata", () => {
            
			
            // song volume
            // if none is set, make it 1
			if(audioTag.matches("[volume]")){
				if(audioTag.getAttribute("volume").trim() !== ""){
					audioVol = parseInt(audioTag.getAttribute("volume").replace(/[^\d\.]*/g,"")) / 100;
					audioTag.volume = audioVol
				}
			} else {
                audioVol = 1;
            }
            
            // create "current time" displayer			
			let curDur = document.createElement("div");
			curDur.classList.add("q-current-time");
			curDur.textContent = "0:00";
			addCtrls.after(curDur)
            
            // at start, set current time to 0
			audioTag.setAttribute("cur-time",0);
			
            // create "total length" display
			let totalDur = document.createElement("div");
			totalDur.classList.add("q-duration");
			totalDur.textContent = durFormatted(audioTag.duration);
			newAudCont.append(totalDur);
            
            // get and set total duration
            audioDur = audioTag.duration;
			audioTag.setAttribute("dur",audioTag.duration);
			
            // get ratio of the vals between:
            // A. song duration
            // B. width of slider
			widthToSongRatio = audioTag.duration / sliderKnobCont.offsetWidth;
			
            // when song has loaded, allow UI
			canDrag()
		})
		
		/*----- TIME-UPDATE -----*/
		audioTag.addEventListener("timeupdate", () => {
			audioTag.setAttribute("cur-time",audioTag.currentTime);
            // current time - progress (%)
			let percenDot = audioTag.currentTime / audioDur;			
			let percen = percenDot * 100;
			
			newAudCont.style.setProperty("--Quick-Music-Player-Fill-Percentage",`${percen}%`);
			
			let audPrev = audioTag.previousElementSibling;
            // update "current-time" display
			audPrev !== null && audPrev.matches(".q-current-time") ? audPrev.textContent = durFormatted(audioTag.currentTime) : ""
		})
		
		/*----- PLAY/PAUSE CLICK -----*/
		addCtrls.addEventListener("click", () => {
			audioTag.paused ? audioTag.play() : audioTag.pause();
		})
		
		/*----- PLAY EVENT -----*/
		audioTag.addEventListener("play", () => {
			playBtn.style.display = "none";
			pauseBtn.style.display = "block";
		})
		
		/*----- PAUSE EVENT -----*/
		audioTag.addEventListener("pause", () => {
			playBtn.style.display = "block";
			pauseBtn.style.display = "none";
		})
		
		/*----- SONG ENDED -----*/
		audioTag.addEventListener("ended", () => {
			playBtn.style.display = "block";
			pauseBtn.style.display = "none";
			audioTag.currentTime = 0;
		})
		
		/*----- JUMP / DRAG SLIDER -----*/
		let isDragging = false;
		let mouseDown = false;
		
		function canDrag(){
			
			sliderKnobCont.addEventListener("mousedown", (e) => {

				if(e.buttons === 1){
					isDragging = false;
					mouseDown = true;
					
					audioTag.volume = 0;
					audioTag.paused ? audioTag.play() : "";

					sliderKnobCont.addEventListener("mousemove", (e) => {
						isDragging = true;

						if(isDragging && mouseDown){

							let newTime = e.offsetX * widthToSongRatio;
							audioTag.currentTime = newTime;
							audioTag.volume = 0;
							audioTag.paused ? audioTag.play() : "";


							sliderKnobCont.addEventListener("mouseleave", () => {
								// stop out-of-bounds
								isDragging = false;
							})//end mouseover
						}//end if both true				
					})//end dragging
					
					sliderKnobCont.addEventListener("mouseup", (e) => {
						let wasDragging = isDragging;

						isDragging = false;
						mouseDown = false;

						// if no drag happened,
						// just update the user on where they
						// released their mouse
						if(!wasDragging){						
							let newTime = e.offsetX * widthToSongRatio;
							audioTag.currentTime = newTime;
							audioTag.volume = audioVol;
							audioTag.paused ? audioTag.play() : "";
						}
					});//end mouseup
				}//end: if mousedown is LEFT, NOT RIGHT
				
			})//end mousedown
		}//end canDrag()
		
        // update song duration v. slider width ratio
		window.addEventListener("resize", () => {
			widthToSongRatio = audioDur / sliderKnobCont.offsetWidth;
		})
		
		/*----- TIMESTAMP FORMATTER -----*/
		function durFormatted(sometime){
			let aud_dur = sometime;
			let MO1 = "0" + Math.floor(aud_dur / 60);
			let SO1 = "0" + (Math.floor(aud_dur) - MO1 * 60);
			let durmat = MO1.substr(-2) + ":" + SO1.substr(-2);
			return durmat
		}
	}) : ""
}
